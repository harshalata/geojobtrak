package com.sarvaha.jobtrak;

import java.io.Serializable;
import java.util.List;

/**
 * Created by harshalata on 16/9/15.
 */
public class GroupItemDetail implements Serializable {

    private int id;
    private String pierNumber;
    private String pierState;
    private String mandrelBottom;
    private String actualGrout;
    private String actualCone;
    private String remarks;
    private String date;
    private String layoutType;

    public GroupItemDetail() {
        this.pierNumber = "";
        this.pierState = "";
        this.mandrelBottom = "";
        this.actualGrout = "";
        this.actualCone = "";
        this.remarks = "";
        this.date = "";
    }
    public GroupItemDetail(String pierNumber, String pierState, String mandrelBottom, String actualGrout, String actualCone, String remarks, String date, String layoutType) {
        this.pierNumber = pierNumber;
        this.pierState = pierState;
        this.mandrelBottom = mandrelBottom;
        this.actualGrout = actualGrout;
        this.actualCone = actualCone;
        this.remarks = remarks;
        this.date = date;
        this.layoutType = layoutType;
    }

    public void setPierNumber(String pierNumber) {
        this.pierNumber = pierNumber;
    }
    public String getPierNumber() {
        return pierNumber;
    }

    public void setPierState(String pierState) {
        this.pierState = pierState;
    }
    public String getPierState() {
        return pierState;
    }

    public void setMandrelBottom(String mandrelBottom) {
        this.mandrelBottom = mandrelBottom;
    }
    public String getMandrelBottom() {
        return mandrelBottom;
    }

    public void setActualGrout(String actualGrout) {
        this.actualGrout = actualGrout;
    }
    public String getActualGrout() {
        return actualGrout;
    }

    public void setActualCone(String actualCone) {
        this.actualCone = actualCone;
    }
    public String getActualCone() {
        return actualCone;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
    public String getRemarks() {
        return remarks;
    }

    public void setDate(String date) {
        this.date = date;
    }
    public String getDate() {
        return date;
    }

    public void setLayoutType(String layoutType) {
        this.layoutType = layoutType;
    }
    public String getLayoutType() {
        return layoutType;
    }
}
