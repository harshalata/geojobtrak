package com.sarvaha.jobtrak;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class ForgotPasswordActivity extends AppCompatActivity {

    //Set the URL
    //public final static String apiURL = "http://192.168.1.29:8080/geojobtrak-web/account/forgetPassword";
    public final static String apiURL = "https://portal.geojobtrack.com/account/forgetPassword";
    //public final static String apiURL = "https://stage-geojobtrack.aws.af.cm/account/forgetPassword";
    //Declare the inputs variables
    Button btnReset;
    EditText txtemail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        //Get the elements by id
        btnReset=(Button)findViewById(R.id.button);
        txtemail=(EditText)findViewById(R.id.editText);

        //On click RESET button
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!txtemail.getText().toString().isEmpty()) {

                    //Call REST service
                    String urlString = apiURL;
                    new RESTWebservice().execute(urlString);
                }
                else
                {
                    //Display error message
                    Toast.makeText(getApplicationContext(), "Please enter an email address.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_forgot_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_back) {
            Intent intent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
            startActivity(intent);
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //Display alert message when back button has been pressed
        login();
        return;
    }

    public void gotoBackPage(View v) {
        login();
    }

    public void login() {
        //Redirect to Login activity
        Intent intent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
        finish();
        startActivity(intent);
    }

    private class RESTWebservice extends AsyncTask<String, String, String> {
        //Create progress dialog
        private ProgressDialog Dialog = new ProgressDialog(ForgotPasswordActivity.this);
        //Declare variables
        String userData = "";
        private String resultToDisplay = "";
        private String Error = null;

        @Override
        protected void onPreExecute() {

            //Start Progress Dialog (Message)
            Dialog.setMessage("Please wait..");
            Dialog.show();

            // Set Request parameter
           userData = "email=" + txtemail.getText().toString();
        }

        @Override
        protected String doInBackground(String... params) {

            // URL to call
            String urlString = params[0];
            BufferedReader reader=null;
            HttpURLConnection urlConnection = null;

            // HTTP POST
            try {
                //Set URL
                URL url = new URL(urlString);
                //Send POST data Request
                urlConnection = (HttpURLConnection) url.openConnection();
                //http POST method is used
                urlConnection.setDoOutput(true);
                //Set URL content type
                urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                //set URL parameters
                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(userData);
                wr.flush();

                // Get the server response
                reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while((line = reader.readLine()) != null)
                {
                    // Append server response in string
                    sb.append(line + "  ");
                }

                // Append Server Response To String
                resultToDisplay = sb.toString();

            } catch (Exception e ) {
                System.out.println(e.getMessage());
                Error = e.getMessage();
            }
            finally
            {
                try
                {
                    reader.close();
                } catch(Exception ex) {}
            }

            return null;

        }


        @Override
        protected void onPostExecute(String res) {
            // Close progress dialog
            Dialog.dismiss();

            if (Error != null) {

            } else {
                try {
                    //Create JSON object from response
                    JSONObject jsonRootObject = new JSONObject(resultToDisplay);
                    //Get status
                    boolean status = Boolean.parseBoolean(jsonRootObject.optString("success"));
                    //if request is failed
                    if(status == false)
                    {
                        //Display error
                        String err = jsonRootObject.optString("errors");
                        Toast.makeText(getApplicationContext(), err, Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        //Intent intent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                        //Display success message
                        Toast.makeText(getApplicationContext(), "Your request has been sent successfully.", Toast.LENGTH_SHORT).show();
                        //startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
    } // end CallAPI
}
