package com.sarvaha.jobtrak;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.widget.SlidingPaneLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Range;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Select;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class JobDetailsActivity extends AppCompatActivity {

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    ArrayList<AcordionGroup> listDataHeader;
    HashMap<AcordionGroup, List<GroupItemDetail>> listDataChild;

    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences sharedPreferences;
    //Get panel
    LinearLayout panel;
    //Set index for panel children
    int index = 3;
    //Initialize dates array
    ArrayList<String> dates = new ArrayList<String>();
    ArrayList<String> dfrDates = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_details);

        RelativeLayout root = (RelativeLayout) findViewById(R.id.main_layout);
        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RelativeLayout submenu = (RelativeLayout) findViewById(R.id.sub_menu);
                if (submenu.getVisibility() == View.VISIBLE) {
                    submenu.setVisibility(View.INVISIBLE);
                }
            }
        });

        Typeface font = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");

        TextView home = (TextView) findViewById(R.id.action_home_button);
        home.setTypeface(font);

        TextView settings = (TextView) findViewById(R.id.action_job_details_settings);
        settings.setTypeface(font);

        TextView pdf = (TextView) findViewById(R.id.pdf);
        pdf.setTypeface(font);

        TextView sync = (TextView) findViewById(R.id.sync);
        sync.setTypeface(font);

        TextView download = (TextView) findViewById(R.id.download);
        download.setTypeface(font);

        TextView marked = (TextView) findViewById(R.id.correct);
        marked.setTypeface(font);

        TextView email = (TextView) findViewById(R.id.message);
        email.setTypeface(font);
        //Date timer1 = new Date();
        // get the listview
        //expListView = (ExpandableListView) findViewById(R.id.piers__daily_activity_listview);
        panel = (LinearLayout) findViewById(R.id.list_layout);
        //Get the pass data from previous activity
        sharedPreferences = getSharedPreferences(JobDetailsActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        String JobDetails = sharedPreferences.getString("JobDetails",null);
        String revision = sharedPreferences.getString("revision", null);
        String name = sharedPreferences.getString("name", null);
        String role = sharedPreferences.getString("role", null);
        int userId = sharedPreferences.getInt("userId", 0);
        String jobType = sharedPreferences.getString("jobType", null);
        Long timer1 = sharedPreferences.getLong("timer1", 0);
/*int userId = getIntent().getIntExtra("userId", 0);
        String jobType = getIntent().getStringExtra("jobType");*/

        //Get textview by id
        TextView txtrevision = (TextView) findViewById(R.id.job_revision);
        //Set the text for revision
        txtrevision.setText("Job Revision: "+revision);
        //Get textview for overview
        TextView txtoverview = (TextView) findViewById(R.id.action_overview);
        //set text for overview
        txtoverview.setText("Overview: " + name);

        try{

            piersTypeCount(JobDetails);

            //Date timer2 = new Date();

            dailyActivityAccordion(JobDetails);

            //Date timer3 = new Date();
            // get the listview
            expListView = (ExpandableListView) findViewById(R.id.lvExp);
            listAdapter = new ExpandableListAdapter(this.getApplicationContext(), listDataHeader, listDataChild);
            // setting list adapter
            expListView.setAdapter(listAdapter);
            expListView.setChildDivider(new ColorDrawable(this.getResources().getColor(R.color.vertical_line)));
            expListView.setDividerHeight(1);
            if(listDataHeader.size() != 0) {
                setListViewHeight(expListView);
            }
            expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

                @Override
                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                    setListViewHeight(parent, groupPosition);
                    return false;
                }
            });


            //Set visibilty for tools buttons
            if((role.equals("ROLE_QC") && jobType.equals("Assigned")) || (role.equals("ROLE_OC_SUPERVISOR") && jobType.equals("Other"))) {
                //get button
                Button fieldReport = (Button)findViewById(R.id.field_report);
                //set visibility true
                fieldReport.setVisibility(View.VISIBLE);
                //get button
                Button bulkUpdate = (Button)findViewById(R.id.bulk_update);
                //set visibility true
                bulkUpdate.setVisibility(View.VISIBLE);
                //get button
                Button recordPier = (Button)findViewById(R.id.record_pier);
                //set visibility true
                recordPier.setVisibility(View.VISIBLE);
                //get button
                Button jobSetup = (Button)findViewById(R.id.job_setup);
                //set visibility true
                jobSetup.setVisibility(View.VISIBLE);

            } else if((role.equals("ROLE_QC") || role.equals("ROLE_READ_ONLY")) && jobType.equals("Other")) {
                //get button
                Button fieldReport = (Button)findViewById(R.id.field_report);
                //set visibility false
                fieldReport.setVisibility(View.GONE);
                //get button
                Button bulkUpdate = (Button)findViewById(R.id.bulk_update);
                //set visibility false
                bulkUpdate.setVisibility(View.GONE);
                //get button
                Button recordPier = (Button)findViewById(R.id.record_pier);
                //set visibility true
                recordPier.setVisibility(View.VISIBLE);
                //get button
                Button jobSetup = (Button)findViewById(R.id.job_setup);
                //set visibility true
                jobSetup.setVisibility(View.VISIBLE);

            }
            //Date timer2 = new Date();
            //showPopup("Timer", "[" + (timer2.getTime() - timer1) + "]");
           // Date timer7 = new Date();
            //showPopup("Timer", "Piercount: "+"["+(timer2.getTime() - timer1.getTime())+" Daily Activity: "+(timer3.getTime() - timer2.getTime())+ " Header: " +t1 + " List: " +t2 + "]" + "Tools: " + (timer7.getTime() - timer3.getTime()));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setListViewHeight(ExpandableListView listView) {
        ExpandableListAdapter listAdapter = (ExpandableListAdapter) listView.getExpandableListAdapter();
        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            View listItem = listAdapter.getGroupView(i, false, null, listView);
            listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private void setListViewHeight(ExpandableListView listView, int group) {
        try {
            ExpandableListAdapter listAdapter = (ExpandableListAdapter) listView.getExpandableListAdapter();
            int totalHeight = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < listAdapter.getGroupCount(); i++) {
                View groupItem = listAdapter.getGroupView(i, false, null, listView);
                groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += (groupItem.getMeasuredHeight());
                if (((listView.isGroupExpanded(i)) && (i != group)) || ((!listView.isGroupExpanded(i)) && (i == group))) {
                    for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                        Boolean isLastChild = (j == (listAdapter.getChildrenCount(i) - 1))? true : false;
                        View listItem = listAdapter.getChildView(i, j, isLastChild, null, listView);
                        listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                        totalHeight += (listItem.getMeasuredHeight() + 8);
                    }
                }
            }

            ViewGroup.LayoutParams params = listView.getLayoutParams();
            int height = totalHeight - (listView.getDividerHeight() * (listAdapter.getGroupCount())); //+
        /*if (height < 10)
            height = 200;*/
            params.height = height;
            listView.setLayoutParams(params);
            listView.requestLayout();
        } catch(Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_job_datails, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        //int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //int userId = getIntent().getIntExtra("userId",0);
        String offlineUserData = "";
        try{
            /*//Open the user database
            SQLiteDatabase userdatabase = openOrCreateDatabase("GeoJobTrak", MODE_PRIVATE, null);
            //If open the user database
            if(userdatabase != null) {

                //Set the query
                String qry = "SELECT * FROM UserData WHERE UserId="+userId;
                //Select the user
                Cursor resultSet = userdatabase.rawQuery(qry,null);
                //Get the first record
                if(resultSet.moveToFirst()) {
                    //Get the user details
                    offlineUserData = resultSet.getString(4);
                }
                userdatabase.close();*/
            /*Users user = new Select().from(Users.class).where("UserId = ?", userId).executeSingle();
            offlineUserData = user.userDetails;
            //if offline data is available
            if(offlineUserData != "") {*/

                //Redirect to dashboard activity
                Intent intent = new Intent(JobDetailsActivity.this, dashboardActivity.class);
                finish();
                //intent.putExtra("userJson", offlineUserData);
                startActivity(intent);

            //}
            //}
        } catch(Exception e) {
            e.printStackTrace();
        }
        super.onBackPressed();
    }


    public void gotoHome(View v) {
        onBackPressed();
    }

    /*

     */
    public void piersTypeCount(String JobDetails) {
        try {
            //Create a JSON object
            JSONObject jsonRootObject = new JSONObject(JobDetails);

            //Get the instance of JSONArray that contains JSONObjects
            JSONArray piers = jsonRootObject.optJSONArray("pierList");
            //Get the listview by id
            ExpandableHeightListView listView = (ExpandableHeightListView)findViewById(R.id.piers_listview);
            listView.setExpanded(true);
            //listView.setScrollingCacheEnabled(false);
            //listView.setAnimationCacheEnabled(false);

            // create the grid item mapping
            String[] from = new String[] {"rowid","type","count"};
            int[] to = new int[] { R.id.item_id, R.id.pier_type, R.id.pier_count};

            String[] fromHeader = new String[100];
            int[] toHeader = new int[100];

            // prepare the list of all records
            List<HashMap<String, String>> piersCountList = new ArrayList<HashMap<String, String>>();

            //Initialize the variables for count
            ArrayList<String> piersNotBuilt = new ArrayList<String>();
            ArrayList<String> piersBuilt = new ArrayList<String>();
            ArrayList<String> obstructedPiers = new ArrayList<String>();
            ArrayList<String> deletedPiers = new ArrayList<String>();
            ArrayList<String> addedPiers = new ArrayList<String>();
            ArrayList<String> bstPiers = new ArrayList<String>();

            //Iterate the pier list
            for(int i=0;i<piers.length();i++){
                //Get the ith object
                JSONObject pierObj = piers.getJSONObject(i);

                //Initialize the variables
                float bstDepth = 0;
                String bstDeflection = "";
                String fieldStatus = "";

                //Set flags for keys are exist or not
                Boolean bstDepthFlag = false;
                Boolean bstDeflectionFlag = false;
                Boolean fieldStatusFlag = false;

                //Get pier number
                String pierNumber = pierObj.optString("pierNumber");
                //Get design status
                String designStatus = pierObj.optString("designStatus");
                //Get pier origin
                String pierOrigin = pierObj.optString("pierOrigin");
                //Iterator for piers object keys
                Iterator<String> iter = pierObj.keys();
                //Iterate through piers object keys
                while(iter.hasNext() && (!bstDepthFlag || !bstDeflectionFlag || !fieldStatusFlag)) {
                    //Get next key
                    String key = iter.next();
                    //key is bstDepth
                    if(key.equals("bstDepth")) {
                        //Get bstDepth
                        bstDepth = Float.parseFloat(pierObj.optString("bstDepth"));
                        bstDepthFlag = true;
                    }
                    //if key is bstDeflection
                    if(key.equals("bstDeflection")) {
                        //Get bstDeflection
                        bstDeflection = pierObj.optString("bstDeflection");
                        bstDeflectionFlag = true;
                    }
                    //if key is field Status
                    if(key.equals("fieldStatus")) {
                        //Get field status
                        fieldStatus = pierObj.optString("fieldStatus");
                        fieldStatusFlag = true;
                    }
                }
                //if field status is NOT INSTALLED and design status is not DELETED FROM DESIGN
                if(fieldStatus.equals("NOT_INSTALLED") && !designStatus.equals( "DELETED_FROM_DESIGN")) {
                    //Increment piers not build count
                    piersNotBuilt.add(pierNumber);
                }
                //if field status is INSTALLED or INSTALLED DATA OUTSTANDING
                if(fieldStatus.equals("INSTALLED") || fieldStatus.equals("INSTALLED_DATA_OUTSTANDING")) {
                    //Increment piers built count
                    piersBuilt.add(pierNumber);
                }
                //if field status is OBSTRUCTED
                if(fieldStatus.equals("OBSTRUCTED")) {
                    //Increment obstructed piers count
                    obstructedPiers.add(pierNumber);
                }
                //if field status is DELETED
                if(fieldStatus.equals("DELETED")) {
                    //Increment deleted piers count
                    deletedPiers.add(pierNumber);
                }
                //if piers origin is FIELD
                if(pierOrigin.equals("FIELD")) {
                    //Increment added piers count
                    addedPiers.add(pierNumber);
                }
                //if bstDepth and bstDeflection values are exist
                if(bstDepth != 0 && !bstDeflection.isEmpty()) {
                    //Increment the bst piers count
                    bstPiers.add(pierNumber);
                }
            }
            // create the list items array
            String[] pierType = new String[] {"Total Piers","Piers Not Built","Piers Built","Obstructed Piers","Deleted Piers","Added Piers","BST Piers"};
            int[] pierCount = new int[] {piers.length(), piersNotBuilt.size(), piersBuilt.size(), obstructedPiers.size(), deletedPiers.size(), addedPiers.size(), bstPiers.size()};
            //piers.length()
            String [] pierList = new String[] { null, piersNotBuilt.toString(), piersBuilt.toString(), obstructedPiers.toString(),deletedPiers.toString(),addedPiers.toString(),bstPiers.toString()};

            //Iterate list items array
            for(int i=0;i<pierCount.length;i++) {
                //Create HashMap
                HashMap<String, String> listitem = new HashMap<String, String>();

                //Add fields in items
                listitem.put("rowid", pierList[i]);
                listitem.put("type", pierType[i]);
                listitem.put("count", "" + pierCount[i] + "");

                piersCountList.add(listitem);
            }
            // fill in the grid_item layout
            PierListAdapter adapter = new PierListAdapter(this, piersCountList, R.layout.listview_grid, from, to);
            adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            //set adapter for listview
            listView.setAdapter(adapter);
            ColorDrawable divColor = new ColorDrawable(this.getResources().getColor(R.color.vertical_line));
            listView.setDivider(divColor);
            listView.setDividerHeight(1);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                    //Toast.makeText(getApplicationContext(), "onItemClick.", Toast.LENGTH_LONG).show();
                    LinearLayout pierLayout = (LinearLayout) v;
                    TextView txtPiers = (TextView) pierLayout.getChildAt(3);
                    TextView txtType = (TextView) pierLayout.getChildAt(0);
                    if (!txtPiers.getText().toString().isEmpty()) {
                        showPopup(txtType.getText().toString(), txtPiers.getText().toString());
                    }
                }
            });
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void showPopup(String title, String pierNum) {
        if(!pierNum.equals("[]")) {
            AlertDialog.Builder piersPopup = new AlertDialog.Builder(JobDetailsActivity.this);
            // Setting Dialog Title
            if(title.equals("Piers Not Built")) {
                title = "New Piers";
            } else  if(title.equals("Piers Built")) {
                title = "Built Piers";
            } else  if(title.equals("Added Piers")) {
                title = "Field Created Piers";
            } else  if(title.equals("BST Piers")) {
                title = "BST Marked Piers";
            }
            piersPopup.setTitle(title);
            // Setting Dialog Message
            piersPopup.setMessage(pierNum.replaceAll("\\[|\\]",""));
            // Setting Icon to Dialog
            //logputDialog.setIcon(R.drawable.dialog_icon);
            // Setting Positive "OK" Button
            piersPopup.setNegativeButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to invoke NO event
                            dialog.cancel();
                        }
                    });
            // Showing Alert Message
            piersPopup.show();
        }
    }

    /*
     *  Function for hide show submenu on click settings button
     */
    public void hideShow(View v) {
        //Get submenu latout
        final RelativeLayout submenu=(RelativeLayout) findViewById(R.id.sub_menu);
        //if submenu is invisible
        if(submenu.getVisibility() == View.INVISIBLE) {
            //set submenu visible
            submenu.setVisibility(View.VISIBLE);
            submenu.bringToFront();
        } else {
            //set submenu invisible
            submenu.setVisibility(View.INVISIBLE);
        }
    }

    public void dailyActivityAccordion(String JobDetails) {
        try {
            //Create a JSON object
            JSONObject jsonRootObject = new JSONObject(JobDetails);

            //Get the instance of JSONArray that contains JSONObjects of pier history
            JSONArray pierHistory = jsonRootObject.optJSONArray("pierHistory");
            //Get the instance of JSONArray that contains JSONObjects of dfr list
            JSONArray dfrList = jsonRootObject.optJSONArray("dfrList");
            //Create a dates array for no. of accordion
            getDates(pierHistory, dfrList);

            Collections.sort(dates, Collections.reverseOrder());

            //Date timer4, timer5, timer6;
            //String t1 = "", t2 = "";
            // preparing list data
            prepareListData(dates,pierHistory);
            /*//fetch dates
            if(dates.size() != 0) {

                for(int i=0; i<=dates.size()-1; i++) {
                    //timer4 = new Date();
                    //Create header params:dayCount, timestamp
                    createHeader(i+1, dates.get(i));
                    //timer5 = new Date();
                    //t1 = t1 + (timer5.getTime() - timer4.getTime()) + " ";
                    //Create a list for daily activity
                    createDailyActivityList(pierHistory,dates.get(i));
                    //timer6 = new Date();
                    // t2 = t2 + (timer6.getTime() - timer5.getTime())  + " ";
                }
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /*
     * Preparing the list data
     */
    private void prepareListData(ArrayList<String> dates , JSONArray pierHistory) {
        listDataHeader = new ArrayList<AcordionGroup>();
        listDataChild = new HashMap<AcordionGroup, List<GroupItemDetail>>();
        try {
            //fetch dates
            if(dates.size() != 0) {

                for(int i=0; i<=dates.size()-1; i++) {
                    //Create header params:dayCount, timestamp
                    createAccordionHeader(i + 1, dates.get(i), pierHistory);
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static JSONArray sortPierHistoryArray(JSONArray array) {

        List<JSONObject> jsons = new ArrayList<JSONObject>();

        try{

            for (int i = 0; i < array.length(); i++) {
                jsons.add(array.getJSONObject(i));
            }
            Collections.sort(jsons, new Comparator<JSONObject>() {
                @Override
                public int compare(JSONObject lhs, JSONObject rhs) {
                    String timestamp1 = lhs.optString("timestamp");
                    String timestamp2 = rhs.optString("timestamp");

                    return timestamp1.compareTo(timestamp2);
                }
            });

        } catch(JSONException e) {

        }
        return new JSONArray(jsons);
    }

    public static JSONArray sortDfrArray(JSONArray array) {

        List<JSONObject> jsons = new ArrayList<JSONObject>();

        try{

            for (int i = 0; i < array.length(); i++) {
                jsons.add(array.getJSONObject(i));
            }
            Collections.sort(jsons, new Comparator<JSONObject>() {
                @Override
                public int compare(JSONObject lhs, JSONObject rhs) {
                    String timestamp1 = lhs.optString("dfrCreatedDate");
                    String timestamp2 = rhs.optString("dfrCreatedDate");
                    return timestamp1.compareTo(timestamp2);
                }
            });

        } catch(JSONException e) {

        }
        return new JSONArray(jsons);
    }

    public void getDates(JSONArray pierHistory, JSONArray dfrList) {
        try{
            int k = 0;
            Boolean flag = false;
            //Iterate the dfrList
            if(dfrList.length() != 0) {
                for(int i=0;i<dfrList.length() ;i++) {
                    //Get the ith object
                    JSONObject dfrObj = dfrList.getJSONObject(i);
                    //Get date
                    String timestamp = dfrObj.optString("dfrCreatedDate");
                    Date date1 = new Date(Long.parseLong(timestamp));
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                    String formattedDate1 = sdf.format(date1);

                    if(dates.size() != 0) {
                        //Fetch the date array for insert the dfrDate
                        for(k=0; k<dates.size() && !flag; k++) {
                            Date date2 = new Date(Long.parseLong(dates.get(k)));
                            String formattedDate2 = sdf.format(date2);
                            if(formattedDate2.equals(formattedDate1)) {
                                flag = true;
                            }

                        }
                        if (k == dates.size() && !flag) {
                            dates.add(timestamp);
                            dfrDates.add(timestamp);
                        }
                    } else {
                        dates.add(timestamp);
                        dfrDates.add(timestamp);
                    }
                }
            }

            if(pierHistory != null) {
                //Iterate the pier history array
                for(int i=0;i<pierHistory.length();i++) {

                    flag = false;
                    //Get the ith object
                    JSONObject historyObj = pierHistory.getJSONObject(i);

                    String timestamp = historyObj.optString("timestamp");
                    Date date1 = new Date(Long.parseLong(timestamp));
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                    String formattedDate1 = sdf.format(date1);

                    if(dates.size() != 0) {
                        //Fetch the date array for insert the history date
                        for(k=0;k<dates.size() && !flag;k++) {
                            Date date2 = new Date(Long.parseLong(dates.get(k)));
                            String formattedDate2 = sdf.format(date2);
                            if(formattedDate2.equals(formattedDate1)) {
                                flag = true;
                            }
                        }
                        if (k == dates.size() && !flag) {
                            dates.add(timestamp);
                        }
                    } else {
                        dates.add(timestamp);
                    }
                }
            }

            } catch(Exception e) {
            e.printStackTrace();
        }

    } //getDates

    public void createHeader(int dayCount, String timestamp) {
        // prepare the list of all records
        List<HashMap<String, String>> headerList = new ArrayList<HashMap<String, String>>();

        ListView dailyHeaderList = new ListView(this);

        String [] fromHeader = new String[] {"title", "dfr","date","day"};
        int [] toHeader = new int[] { R.id.accordionHeader, R.id.dfr, R.id.accordionDate, R.id.accordionDay};

        Boolean dfr;
        Date date1 = new Date(Long.parseLong(timestamp));
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        String formattedDate = sdf.format(date1);

        HashMap<String, String> header = new HashMap<String, String>();
        String title = "Day "+dayCount+" Activity - "+ formattedDate;
        header.put("title",title);
        //Button button = (Button) findViewById(R.id.dfr);
        dfr = isDfrDate(timestamp);
        if(dfr) {
            header.put("dfr", " View DFR  ");
        } else {
            header.put("dfr", "Publish DFR");
        }

        header.put("date",timestamp);
        header.put("day", "" + dayCount);
        headerList.add(header);

        // fill in the grid_item layout
        SimpleAdapter headerAdapter = new SimpleAdapter(getBaseContext(), headerList, R.layout.acordion_group_header, fromHeader, toHeader);
        headerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        //set adapter for listview
        dailyHeaderList.setAdapter(headerAdapter);
        ColorDrawable divColor = new ColorDrawable(this.getResources().getColor(R.color.vertical_line));
        dailyHeaderList.setDivider(divColor);
        dailyHeaderList.setDividerHeight(1);
        //dailyHeaderList.setScrollingCacheEnabled(false);
        //dailyHeaderList.setAnimationCacheEnabled(false);

        TextView textView = new TextView(this);
        textView.setHeight(10);
        textView.setBackgroundColor(Color.parseColor("#c5ccd4"));

        dailyHeaderList.addFooterView(textView);
        dailyHeaderList.setItemsCanFocus(true);

        dailyHeaderList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getApplicationContext(), "onItemClick.", Toast.LENGTH_LONG).show();
                LinearLayout layout = (LinearLayout) findViewById(R.id.list_layout);
                int i = layout.indexOfChild(parent) + 1;
                ExpandableHeightListView list = (ExpandableHeightListView) layout.getChildAt(i);
                if(list != null) {
                    if (list.getVisibility() == View.VISIBLE) {
                        list.setVisibility(View.GONE);
                    } else {
                        list.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        panel.addView(dailyHeaderList, index);
        index++;

    } //createHeader

    public Boolean isDfrDate(String timestamp) {
        //Fetch the date array for insert the history date
        if(dfrDates.size() != 0) {
            Date date1 = new Date(Long.parseLong(timestamp));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            String formattedDate1 = sdf.format(date1);

            for(int k=0;k<dfrDates.size();k++) {
                Date date2 = new Date(Long.parseLong(dfrDates.get(k)));
                String formattedDate2 = sdf.format(date2);
                if(formattedDate2.equals(formattedDate1)) {
                    return true;
                }
            }
        }
        return false;
    } //isDfrDate

    public void createDailyActivityList(JSONArray pierHistory, String timestamp) {

        try{
            // create the grid item mapping
            String [] from = new String[] {"pierNum", "built", "depth","grout","cone","remarks"}; //, "built"
            int [] to = new int[] { R.id.pier_no, R.id.pier_built, R.id.pier_depth, R.id.pier_grout, R.id.pier_cone, R.id.pier_remarks}; //R.id.pier_built,

            int piersBuiltCount = 0;
            int builtLength = 0;
            Boolean flagHeader = false;
            // prepare the list of all records
            List<HashMap<String, String>> dayActivityList = new ArrayList<HashMap<String, String>>();
            ExpandableHeightListView dailyListView = new ExpandableHeightListView(this);
            //Set background color for list
            dailyListView.setBackgroundColor(Color.WHITE);
            dailyListView.setExpanded(true);
            //ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) dailyListView.getLayoutParams() ;

            Date date2 = new Date(Long.parseLong(timestamp));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            String formattedDate2 = sdf.format(date2);

            if(pierHistory != null) {
                for(int i=0;i<pierHistory.length();i++) {

                    //Get the ith object
                    JSONObject historyObj = pierHistory.getJSONObject(i);

                    String timestampThis = historyObj.optString("timestamp");
                    Date date1 = new Date(Long.parseLong(timestampThis));
                    String formattedDate1 = sdf.format(date1);

                   if(formattedDate1.equals(formattedDate2)) {
                        if(flagHeader == false) {
                            //Create HashMap
                            HashMap<String, String> listItem = new HashMap<String, String>();
                            //Add fields in items
                            listItem.put("pierNum", "Pier#");
                            listItem.put("built", "Built"); //built
                            listItem.put("depth", "Depth");
                            listItem.put("grout", "Grout");
                            listItem.put("cone", "Cone");
                            listItem.put("remarks", "Remarks");
                            //listItem.put("HistoryDate", timestamp);
                            dayActivityList.add(listItem);
                            flagHeader= true;
                        }

                        String pierNum = historyObj.optString("pierNumber");
                        //I:Installed-data-outstanding , O: obstructed, D: Deleted , Y: Installed
                        String pierBuilt = historyObj.optString("pierState");
                        String pierDepth = historyObj.optString("mandrelBottom");
                        String pierGrout = historyObj.optString("actualGrout");
                        String pierCone = historyObj.optString("actualCone");
                        String pierRemarks = historyObj.optString("remarks");
                        String pierOrigin = historyObj.optString("origin");

                        if(pierBuilt.equals("Y") || pierBuilt.equals("I")) {
                            piersBuiltCount = piersBuiltCount + 1;
                            builtLength = builtLength + Integer.parseInt(pierDepth);
                        }

                        //Create HashMap
                        HashMap<String, String> listItem = new HashMap<String, String>();
                        //Add fields in items
                        if(pierOrigin.equals("FIELD")) {
                            pierNum = " * " + pierNum;
                        }
                        listItem.put("pierNum", pierNum);

                        String image = "";
                        if(pierBuilt.equals("Y")) {
                            image = "\uf00c";
                        } else if(pierBuilt.equals("I")) {
                            image = "\uf071";
                        } else if(pierBuilt.equals("O")) {
                            image = "\uf05e";
                        } else if(pierBuilt.equals("D")) {
                            image = "\uf00d";
                        }

                        listItem.put("built", image); //built
                        listItem.put("depth", pierDepth);
                        listItem.put("grout", pierGrout);
                        listItem.put("cone", pierCone);
                        listItem.put("remarks", pierRemarks);
                        listItem.put("HistoryDate", timestamp);

                        dayActivityList.add(listItem);
                    }
                }
            }

            // fill in the grid_item layout
            PierListAdapter listAdapter = new PierListAdapter(this, dayActivityList, R.layout.accordion_list_item, from, to);
            listAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            //set adapter for listview
            dailyListView.setAdapter(listAdapter);
            ColorDrawable divColor = new ColorDrawable(this.getResources().getColor(R.color.vertical_line));
            dailyListView.setDivider(divColor);
            dailyListView.setDividerHeight(1);
            //dailyListView.setScrollingCacheEnabled(false);
            //dailyListView.setAnimationCacheEnabled(false);

            if(pierHistory != null) {
                TextView piersData = new TextView(this);
                piersData.setTextColor(Color.parseColor("#757575"));
                piersData.setText("Total Piers Built: " + piersBuiltCount + "\n Built Length: " + builtLength + "'");
                piersData.setPadding(5,2,2,2);
                dailyListView.addHeaderView(piersData);

                TextView footer = new TextView(this);
                footer.setHeight(10);
                footer.setBackgroundColor(Color.parseColor("#c5ccd4"));
                dailyListView.addFooterView(footer);

                dailyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                        //Toast.makeText(getApplicationContext(), "onItemClick.", Toast.LENGTH_LONG).show();
                    }
                });
            }
            panel.addView(dailyListView, index);
            index++;
        } catch(Exception e) {
            e.printStackTrace();
        }

    } //createDailyActivityList

    public void createAccordionHeader(int dayCount, String timestamp, JSONArray pierHistory) {
        try {
            Boolean dfr;
            Date date1 = new Date(Long.parseLong(timestamp));
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            String formattedDate = sdf.format(date1);
            String title = "Day "+dayCount+" Activity - "+ formattedDate;
            dfr = isDfrDate(timestamp);
            AcordionGroup acordionGroup = new AcordionGroup();
            acordionGroup.setName(title);
            acordionGroup.setDate(formattedDate);
            acordionGroup.setIsDFR(dfr);
            acordionGroup.setDay(dayCount);
            //Create a list for daily activity
            ArrayList<GroupItemDetail> itemList = createAccordionDailyActivityList(pierHistory,timestamp, acordionGroup);
            acordionGroup.setItemList(itemList);

            /*AcordionGroup acordionGroup = new AcordionGroup(title,formattedDate,dfr,dayCount,itemList,textToHeader);*/
            listDataHeader.add(acordionGroup);

            listDataChild.put(listDataHeader.get(dayCount-1), itemList);
        } catch(Exception e) {
            e.printStackTrace();
        }
    } //createHeader

    /*
    createAccordionDailyActivityList
     */

    public ArrayList<GroupItemDetail> createAccordionDailyActivityList(JSONArray pierHistory, String timestamp, AcordionGroup acordionGroup) {

        ArrayList<GroupItemDetail> itemList = new ArrayList<GroupItemDetail>();

        try{
            int piersBuiltCount = 0;
            int builtLength = 0;
            Boolean flagHeader = false;

            Date date2 = new Date(Long.parseLong(timestamp));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            String formattedDate2 = sdf.format(date2);

            if(pierHistory != null) {
                GroupItemDetail item = new GroupItemDetail(null,null,null,null,null,null,null,"header");
                itemList.add(item);
                for(int i=0;i<pierHistory.length();i++) {

                    //Get the ith object
                    JSONObject historyObj = pierHistory.getJSONObject(i);

                    String timestampThis = historyObj.optString("timestamp");
                    Date date1 = new Date(Long.parseLong(timestampThis));
                    String formattedDate1 = sdf.format(date1);

                    if(formattedDate1.equals(formattedDate2)) {
                        if(flagHeader == false) {
                            GroupItemDetail itemDetail = new GroupItemDetail("Pier#","Built","Depth","Grout","Cone","Remarks","HistoryDate","title");
                            itemList.add(itemDetail);
                            flagHeader= true;
                        }

                        String pierNum = historyObj.optString("pierNumber");
                        //I:Installed-data-outstanding , O: obstructed, D: Deleted , Y: Installed
                        String pierBuilt = historyObj.optString("pierState");
                        String pierDepth = historyObj.optString("mandrelBottom");
                        String pierGrout = historyObj.optString("actualGrout");
                        String pierCone = historyObj.optString("actualCone");
                        String pierRemarks = historyObj.optString("remarks");
                        String pierOrigin = historyObj.optString("origin");

                        if(pierBuilt.equals("Y") || pierBuilt.equals("I")) {
                            piersBuiltCount = piersBuiltCount + 1;
                            builtLength = builtLength + Integer.parseInt(pierDepth);
                        }

                        //Create HashMap
                        HashMap<String, String> listItem = new HashMap<String, String>();
                        //Add fields in items
                        if(pierOrigin.equals("FIELD")) {
                            pierNum = " * " + pierNum;
                        }

                        GroupItemDetail itemDetail = new GroupItemDetail(pierNum,pierBuilt,pierDepth,pierGrout,pierCone,pierRemarks,formattedDate2,"other");
                        itemList.add(itemDetail);
                    }
                }
            }

            if(pierHistory != null) {
                /*TextView piersData = new TextView(this);
                piersData.setTextColor(Color.parseColor("#757575"));
                piersData.setText("Total Piers Built: " + piersBuiltCount + "\n Built Length: " + builtLength + "'");
                piersData.setPadding(5, 2, 2, 2);*/
                String str = "Total Piers Built: " + piersBuiltCount + "\n Built Length: " + builtLength + "'";
                //For header
                acordionGroup.setTextToHeader(str);
            }

        } catch(Exception e) {
            e.printStackTrace();
        }
        return itemList;
    }
}
