package com.sarvaha.jobtrak;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by harshalata on 15/9/15.
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    // header titles
    private List<AcordionGroup> _listDataHeader;
    // child data in format of header title, child title
    private HashMap<AcordionGroup, List<GroupItemDetail>> _listDataChild;

    public ExpandableListAdapter(Context context, List<AcordionGroup> listDataHeader, HashMap<AcordionGroup, List<GroupItemDetail>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        int[] colors = new int[] { 0x30FFFFFF, 0x30EEEEEE };
        AcordionGroup acordionGroup = (AcordionGroup) getGroup(groupPosition);
        GroupItemDetail itemDetail = (GroupItemDetail) acordionGroup.getItemList().get(childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.accordion_list_item, parent, false);
        }

        if(itemDetail.getLayoutType() == "header") {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.child_header, parent, false);
            TextView txt = (TextView) convertView.findViewById(R.id.txtHeader);
            txt.setText(acordionGroup.getTextToHeader());
        } else {
            if (itemDetail.getLayoutType() == "title") {
                LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.accordion_list_item, parent, false);
                convertView.setBackgroundColor(0x30CFCFCF);

            } else if (itemDetail.getLayoutType() == "other") {
                LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.accordion_list_item, parent, false);
                int colorPos = childPosition % colors.length;
                convertView.setBackgroundColor(colors[colorPos]);
            }
            TextView txtChildPierNo = (TextView) convertView.findViewById(R.id.pier_no);
            TextView txtChildPierBuilt = (TextView) convertView.findViewById(R.id.pier_built);
            TextView txtChildPierDepth = (TextView) convertView.findViewById(R.id.pier_depth);
            TextView txtChildPierGrout = (TextView) convertView.findViewById(R.id.pier_grout);
            TextView txtChildPierCone = (TextView) convertView.findViewById(R.id.pier_cone);
            TextView txtChildPierRemark = (TextView) convertView.findViewById(R.id.pier_remarks);

            txtChildPierNo.setText(itemDetail.getPierNumber());
            String image = itemDetail.getPierState();
            if(itemDetail.getPierState().equals("Y")) {
                image = "\uf00c";
            } else if(itemDetail.getPierState().equals("I")) {
                image = "\uf071";
            } else if(itemDetail.getPierState().equals("O")) {
                image = "\uf05e";
            } else if(itemDetail.getPierState().equals("D")) {
                image = "\uf00d";
            }
            Typeface font = Typeface.createFromAsset(_context.getAssets(), "fontawesome-webfont.ttf");
            txtChildPierBuilt.setTypeface(font);
            txtChildPierBuilt.setText(image);
            txtChildPierDepth.setText(itemDetail.getMandrelBottom());
            txtChildPierGrout.setText(itemDetail.getActualGrout());
            txtChildPierCone.setText(itemDetail.getActualCone());
            txtChildPierRemark.setText(itemDetail.getRemarks());
        }
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        AcordionGroup acordionGroup = (AcordionGroup) getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.acordion_group_header, parent, false);
        }

        TextView lblGroupHeader = (TextView) convertView.findViewById(R.id.accordionHeader);
        Button button = (Button) convertView.findViewById(R.id.dfr);
        TextView day = (TextView) convertView.findViewById(R.id.accordionDay);
        TextView date = (TextView)convertView.findViewById(R.id.accordionDate);

        lblGroupHeader.setText(acordionGroup.getName());
        if(acordionGroup.getIsDFR()) {
            button.setText("View DFR");
        } else {
            button.setText("Publish DFR");
        }
        date.setText(acordionGroup.getDate());
        day.setText("day "+acordionGroup.getDay());

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
