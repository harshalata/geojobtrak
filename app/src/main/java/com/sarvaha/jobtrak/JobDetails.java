package com.sarvaha.jobtrak;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by harshalata on 7/10/15.
 */
@Table(name = "JobDetails")
public class JobDetails extends Model {

    @Column(name = "JobId")
    public int jobId;

    @Column(name = "UserId")
    public int userId;

    @Column(name = "JobType")
    public String jobType;

    @Column(name = "Jobs")
    public String jobs;

    public JobDetails() {
        super();
    }

    public JobDetails(int jobId,int userId,String jobType,String jobs) {
        super();
        this.jobId = jobId;
        this.userId = userId;
        this.jobType = jobType;
        this.jobs = jobs;
    }
}
