package com.sarvaha.jobtrak;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

/**
 * Created by harshalata on 14/9/15.
 */
public class PierListAdapter extends SimpleAdapter {
    private int[] colors = new int[] { 0x30EEEEEE, 0x30FFFFFF };
    Context _context;


    public PierListAdapter(Context context, List<HashMap<String, String>> items, int resource, String[] from, int[] to) {
        super(context, items, resource, from, to);
        _context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);

        if(view == null){
            LayoutInflater vi = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = vi.inflate(R.layout.accordion_list_item, null);
        }

        if(position == 0) {
            view.setBackgroundColor(0x30CFCFCF);
        } else {
            int colorPos = position % colors.length;
            view.setBackgroundColor(colors[colorPos]);
        }

        Typeface fontFamily = Typeface.createFromAsset(this._context.getAssets(),"fontawesome-webfont.ttf");
        TextView txtbuilt = (TextView) view.findViewById(R.id.pier_built);
        if(txtbuilt != null) {
            txtbuilt.setTypeface(fontFamily);
        }

        return view;
    }

}
