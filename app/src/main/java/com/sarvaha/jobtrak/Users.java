package com.sarvaha.jobtrak;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by harshalata on 7/10/15.
 */
@Table(name = "Users")
public class Users extends Model {

    @Column(name = "Username")
    public String username;

    @Column(name = "Password")
    public String password;

    @Column(name = "UserId")
    public int userId;

    @Column(name = "UserDetails")
    public String userDetails;

    @Column(name = "Role")
    public String role;

    @Column(name = "SessionToken")
    public String sessionToken;

    public Users() {
        super();
    }

    public Users(String username, String password, int userId, String userDetails, String role, String sessionToken) {
        super();
        this.username = username;
        this.password = password;
        this.userId = userId;
        this.userDetails = userDetails;
        this.role = role;
        this.sessionToken = sessionToken;
    }
}
