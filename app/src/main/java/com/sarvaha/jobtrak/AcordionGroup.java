package com.sarvaha.jobtrak;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by harshalata on 16/9/15.
 */
public class AcordionGroup implements Serializable {
    private int id;
    private String name;
    private String date;
    private Boolean isDFR;
    private int day;
    private ArrayList<GroupItemDetail> itemList = new ArrayList<GroupItemDetail>();
    private String textToHeader;

    public AcordionGroup() {
        this.name = "";
        this.date = "";
        this.isDFR = false;
        this.day = 0;
        this.itemList = null;
        textToHeader = "";
    }

    public AcordionGroup(String name, String date, Boolean isDFR, int day, ArrayList<GroupItemDetail> itemList, String textToHeader) {
        this.name = name;
        this.date = date;
        this.isDFR = isDFR;
        this.day = day;
        this.itemList = itemList;
        this.textToHeader = textToHeader;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setDate(String date) {
        this.date = date;
    }
    public String getDate() {
        return date;
    }

    public void setIsDFR(Boolean isDFR) {
        this.isDFR = isDFR;
    }
    public Boolean getIsDFR() {
        return isDFR;
    }

    public void setDay(int day) {
        this.day = day;
    }
    public int getDay() { return day; }

    public void setItemList(ArrayList<GroupItemDetail> itemList) {
        this.itemList = itemList;
    }
    public ArrayList<GroupItemDetail> getItemList() {
        return itemList;
    }

    public void setTextToHeader(String textToHeader) {
        this.textToHeader = textToHeader;
    }
    public String getTextToHeader() {
        return textToHeader;
    }
}
