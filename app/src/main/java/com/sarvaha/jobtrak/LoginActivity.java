package com.sarvaha.jobtrak;

import android.app.AlertDialog;
import android.app.ProgressDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import android.database.sqlite.SQLiteDatabase;
import android.database.Cursor;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;

import org.json.JSONException;
import org.json.JSONObject;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import java.net.HttpURLConnection;
import java.net.URL;

public class LoginActivity extends AppCompatActivity {

    //Set the URL for REST service
    //public final static String apiURL = "http://192.168.1.29:8080/geojobtrak-web/restService/login";
    public final static String apiURL = "https://portal.geojobtrack.com/restService/login";
    //public final static String apiURL = "https://stage-geojobtrack.aws.af.cm/restService/login";
    //Shared References for session storage
    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences sharedpreferences;
    //Declare input variables
    Button btnLogin;
    EditText txtusername,txtpassword;
    TextView lnkForgotPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Get elements by id
        btnLogin=(Button)findViewById(R.id.button);
        txtusername=(EditText)findViewById(R.id.editText);
        txtpassword=(EditText)findViewById(R.id.editText2);
        lnkForgotPassword=(TextView)findViewById(R.id.textView3);

        //On click LOGIN button
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!txtusername.getText().toString().isEmpty() && !txtpassword.getText().toString().isEmpty()) {
                    loginUser();

                } else if (txtusername.getText().toString().isEmpty() && txtpassword.getText().toString().isEmpty()){

                    //Display the error message
                    Toast.makeText(getApplicationContext(), "Please enter the Username and Password", Toast.LENGTH_SHORT).show();

                } else if (txtusername.getText().toString().isEmpty()){

                    //Display the error message
                    Toast.makeText(getApplicationContext(), "Please enter the Username", Toast.LENGTH_SHORT).show();

                }else if (txtpassword.getText().toString().isEmpty()){

                    //Display the error message
                    Toast.makeText(getApplicationContext(), "Please enter the Password", Toast.LENGTH_SHORT).show();

                } else {

                    //Display the error message
                    Toast.makeText(getApplicationContext(), "Wrong Credentials", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //On click Forgot Password link
        lnkForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Redirect to forgot password activity
                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       /* if (id == R.id.action_settings) {
            return true;
        }*/
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //Display alert message when back button has been pressed
        backButtonHandler();
        return;
    }
    /*
        Function will display the confirmation dialog for exit App
     */
    public void backButtonHandler() {

        //Create dialog box for exit from app
        AlertDialog.Builder exitAppDialog = new AlertDialog.Builder(LoginActivity.this);
        // Setting Dialog Title
        exitAppDialog.setTitle("Exit Application?");
        // Setting Dialog Message
        exitAppDialog.setMessage("Are you sure you want to leave the application?");
        // Setting Icon to Dialog
        //exitAppDialog.setIcon(R.drawable.dialog_icon);
        // Setting Positive "Yes" Button
        exitAppDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //Finish the activity
                        finish();
                    }
                });
        // Setting Negative "NO" Button
        exitAppDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // cancel the dialog
                        dialog.cancel();
                    }
                });
        // Showing Alert Message
        exitAppDialog.show();
    }

    /*
        Function call REST web service for login
     */
    public void loginUser() {
        String urlString = apiURL;
        //Call REST web service for authenticate the user
        new RESTWebservice().execute(urlString);
    }

    /*
        Function will get the offline data and user will login as offline user
     */
    public String doOfflineLogin() {
        //initialize offline data
        String offlineUserData = "";
        try {
            //Get username
            String username = txtusername.getText().toString();
            //Get password
            String password = txtpassword.getText().toString();
            //get offline data
            Users user = new Select().from(Users.class).where("Username = ? AND Password = ?", username, password).executeSingle();
            offlineUserData = user.userDetails;
            //Open the user database\
            /*SQLiteDatabase userdatabase = openOrCreateDatabase("GeoJobTrak", MODE_PRIVATE, null);*/
            //If open the user database
           /* if(userdatabase != null) {
                //Set the query
                String qry = "SELECT * FROM UserData WHERE Username='" + username + "' AND Password='" + password + "' ";
                //Select the user
                Cursor resultSet = userdatabase.rawQuery(qry, null);
                //Get the first record
                if (resultSet.moveToFirst()) {
                    //Get the user details
                    offlineUserData = resultSet.getString(4);
                }
                userdatabase.close();
            }*/
        } catch(Exception e) {
            e.printStackTrace();
        }
        return offlineUserData;
    }

    public  void storeUserData(String userResponse) {
        try {
            //Create JSON object from response
            JSONObject jsonRootObject = new JSONObject(userResponse);
            //Store offline data
            //SQLiteDatabase userdatabase = openOrCreateDatabase("GeoJobTrak", MODE_PRIVATE, null);
            //if user database is created
            //if(userdatabase != null) {
                //Drop table
                //userdatabase.execSQL("DROP TABLE IF EXISTS UserData");
                //userdatabase.execSQL("DROP TABLE IF EXISTS JobDetail");
                //String exequery = "";
                //Create table UserData
                //userdatabase.execSQL("CREATE TABLE IF NOT EXISTS UserData(_id INTEGER PRIMARY KEY AUTOINCREMENT, Username TEXT,Password TEXT,UserId INTEGER,UserDetails TEXT,Role TEXT,SessionToken TEXT)");
                //get username
                String username = txtusername.getText().toString();
                //get password
                String password = txtpassword.getText().toString();
                //get userid
                int userId = Integer.parseInt(jsonRootObject.optString("userId"));
                //get session token
                String sessionToken = jsonRootObject.optString("sessionToken");
                //get user role
                String role = jsonRootObject.optString("roles");
                //get user details, jobs
                String userDetails = userResponse.toString();
                //Save user object in database
                Users user = new Users();
                user.username = username;
                user.password = password;
                user.userId = userId;
                user.userDetails = userDetails;
                user.role = role;
                user.sessionToken = sessionToken;
                user.save();

                /*//Set the query
                String qry = "SELECT * FROM UserData WHERE Username='"+username+"' AND Password='"+password+"' ";
                //Select the user
                Cursor resultSet = userdatabase.rawQuery(qry,null);
                //Get the first record
                if(resultSet.moveToFirst()) {
                    //Update database if user is already exist
                    exequery = "UPDATE UserData SET UserDetails='"+userDetails+"', SessionToken='"+sessionToken+"' WHERE Username='"+username+"'";
                    //execute query
                    userdatabase.execSQL(exequery);
                } else {
                    //Insert into database
                    exequery = "INSERT INTO UserData(Username,Password,UserId,UserDetails,Role,SessionToken) VALUES('"+username+"','"+password+"',"+userId+",'"+userDetails+"','"+role+"','"+sessionToken+"')";
                    //execute query
                    userdatabase.execSQL(exequery);
                }*/
                //userdatabase.close();
            //}
        } catch (Exception e) {
            //Display exception
            e.printStackTrace();
        }
    }
    public void displayError(String err) {
        //Display text message
        Toast.makeText(getApplicationContext(), err, Toast.LENGTH_SHORT).show();
    }

    public void redirectTo(String offlineUserData, Boolean offline) {
        //Store the data in local storage
        sharedpreferences = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean("isOfflineUser",offline);
        editor.putString("username", txtusername.getText().toString());
        editor.putString("password",txtpassword.getText().toString());
        editor.putString("userJson", offlineUserData);
        editor.commit();

        //Redirect to dashboard activity
        Intent intent = new Intent(LoginActivity.this, dashboardActivity.class);
        //intent.putExtra("userJson",offlineUserData);
        startActivity(intent);

    }

    private class RESTWebservice extends AsyncTask<String, String, String> {
        //Create a progress dialog
        private ProgressDialog Dialog = new ProgressDialog(LoginActivity.this);
        //Initialize the variable for request params
        String userData = "";
        //Initialize the variable for rest service response
        private String resultToDisplay = "";
        //Initialize the variable for error
        private String Error = null;

        @Override
        protected void onPreExecute() {
            //Start Progress Dialog (Message)
            Dialog.setMessage("Please wait..");
            Dialog.show();
            // Set Request parameter
            userData = "username=" + txtusername.getText().toString() + "&password=" + txtpassword.getText().toString();
        }

        @Override
        protected String doInBackground(String... params) {
            //Get URL to call
            String urlString = params[0];
            //Initialization of variables
            BufferedReader reader = null;
            HttpURLConnection urlConnection = null;
            // HTTP POST
            try {
                //Create url
                URL url = new URL(urlString);
                //Send POST data Request
                urlConnection = (HttpURLConnection) url.openConnection();
                //http POST method is used
                urlConnection.setDoOutput(true);
                //Set URL content type
                urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                //set URL params
                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(userData);
                wr.flush();
                // Get the server response
                reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                // Read Server Response
                while((line = reader.readLine()) != null)
                {
                    // Append server response in string
                    sb.append(line + "  ");
                }
                // Append Server Response To String
                resultToDisplay = sb.toString();
                //resultToDisplay = "{\"error\":\"Page not found\",\"errorCode\":\"404\",\"success\":false}";
            } catch (Exception e ) {
                //store exception in Error variable
                Error = e.getMessage();
            } finally {
                try
                {
                    if(reader != null) {
                        //close reader
                        reader.close();
                    }
                } catch(Exception ex) { ex.printStackTrace(); }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String res) {
            // Close progress dialog
            Dialog.dismiss();
            if (Error != null) {
                //If error, do offline login
                String data = doOfflineLogin();
                if(data != "") {
                    //redirect to dashboard page
                    redirectTo(data,true);
                } else {
                    //display error message
                    displayError(Error);
                }
            } else {
                try {
                    //Create JSON object from response
                    JSONObject jsonRootObject = new JSONObject(resultToDisplay);
                    //Get status
                    boolean status = Boolean.parseBoolean(jsonRootObject.optString("success"));
                    //if request is failed
                    if(status == false)
                    {
                        //get the error code
                        int errorCode = Integer.parseInt(jsonRootObject.optString("errorCode"));
                        if(errorCode != 404){
                            //Display the error message
                            displayError(jsonRootObject.optString("error"));
                        } else {
                            //get the user data after offline login
                            String data = doOfflineLogin();
                            if(data != "") {
                                //redirect to dashboard
                                redirectTo(data,true);
                            } else {
                                //Didn't get offline data, display error message
                                displayError(jsonRootObject.optString("error"));
                            }
                        }
                    } else {
                        //Save the user details after login
                        storeUserData(resultToDisplay);
                        //rediect to dashboard page
                        redirectTo(resultToDisplay,false);
                        //Toast.makeText(getApplicationContext(), "Shared Preferences."+sharedpreferences.getBoolean("isOfflineUser",false), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    //print exception error message
                    e.printStackTrace();
                }
            }
        } //onPostExecute
    } // end CallAPI
}