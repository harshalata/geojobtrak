package com.sarvaha.jobtrak;

import android.app.AlertDialog;
import android.app.ProgressDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import java.net.HttpURLConnection;
import java.net.URL;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class dashboardActivity extends AppCompatActivity {

    //Set the Job URL for REST SERVICE CALL
    //public final static String apiURL = "http://192.168.1.29:8080/geojobtrak-web/restService/getPiersAndDFRByJob";
    public final static String apiURL = "https://portal.geojobtrack.com/restService/getPiersAndDFRByJob";
    //public final static String apiURL = "https://stage-geojobtrack.aws.af.cm/restService/getPiersAndDFRByJob";
    //Set the Login URL for login REST service call
    //public final static String loginURL = "http://192.168.1.29:8080/geojobtrak-web/restService/login";
    public final static String loginURL = "https://portal.geojobtrack.com/restService/login";
    //public final static String loginURL = "https://stage-geojobtrack.aws.af.cm/restService/login";
    //Declare variables for shared preferences
    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences sharedpreferences;

    //Declare the variable for session token
    String sessionToken = "";
    String role = "";
    String jobType = "";
    int userId;
    int jobId;
    int lastVisitedPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        //On click on page
        RelativeLayout root = (RelativeLayout) findViewById(R.id.main_layout);
        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RelativeLayout submenu = (RelativeLayout) findViewById(R.id.sub_menu);
                if (submenu.getVisibility() == View.VISIBLE) {
                    submenu.setVisibility(View.INVISIBLE);
                }
            }
        });
        //Initialize font typeface
        Typeface font = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
        TextView home = (TextView) findViewById(R.id.action_home);
        home.setTypeface(font);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_dashboard, menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
        /*switch (item.getItemId()) {
            case R.id.action_jobsites:
                openJobSites();
                return true;
            case R.id.action_logout:
                //logout();
                return true;
            case R.id.action_job_assigned:
                openJobAssigned();
                return true;
            case R.id.action_job_other:
                openJobOther();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }*/

    }

    @Override
    public void onBackPressed() {
        //Display logout popup when back button has been pressed
        logout();
    }

    public void openJobSites(View v) {

        final RelativeLayout rl=(RelativeLayout) findViewById(R.id.rl);
        rl.removeAllViews();

        final RelativeLayout rl_actionbar=(RelativeLayout) findViewById(R.id.sub_menu);
        if(rl_actionbar.getVisibility() == View.INVISIBLE) {
            rl_actionbar.setVisibility(View.VISIBLE);
            rl_actionbar.bringToFront();
        }
        else
        {
            rl_actionbar.setVisibility(View.INVISIBLE);
        }
    }

    public void logoutUser(View v) {
        logout();
    }

    public void logout() {

        AlertDialog.Builder logputDialog = new AlertDialog.Builder(dashboardActivity.this);
        // Setting Dialog Title
        logputDialog.setTitle("LogOut");
        // Setting Dialog Message
        logputDialog.setMessage("Are you sure you want to logout?");
        // Setting Icon to Dialog
        //logputDialog.setIcon(R.drawable.dialog_icon);
        // Setting Positive "OK" Button
        logputDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        redirectTo();
                    }
                });
        // Setting Negative "Cancel" Button
        logputDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to invoke NO event
                        dialog.cancel();
                    }
                });
        // Showing Alert Message
        logputDialog.show();

    }

    public void redirectTo() {
        sharedpreferences = getSharedPreferences(dashboardActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.commit();
        Intent intent = new Intent(dashboardActivity.this, LoginActivity.class);
        finish();
        startActivity(intent);
    }
    public void deleteData(View v, int id) {

        jobId = id;
        final Button btnDelete = (Button) v;
        AlertDialog.Builder deleteDialog = new AlertDialog.Builder(dashboardActivity.this);
        // Setting Dialog Title
        deleteDialog.setTitle("Delete Job Data");
        // Setting Dialog Message
        deleteDialog.setMessage("Are you sure you want to delete all data of this job?");
        // Setting Icon to Dialog
        //logputDialog.setIcon(R.drawable.dialog_icon);
        // Setting Positive "OK" Button
        deleteDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            //Open the user database
                            /*SQLiteDatabase userdatabase = openOrCreateDatabase("GeoJobTrak", MODE_PRIVATE, null);
                            //If open the user database
                            if(userdatabase != null) {

                                //delete record
                                String exequery = "DELETE FROM JobDetail WHERE JobId="+jobId+" AND UserId="+userId;
                                //execute query
                                userdatabase.execSQL(exequery);
                                userdatabase.close();
                            }*/
                            new Delete().from(JobDetails.class).where("JobId = ? AND UserId = ?", jobId,userId).execute();
                        } catch(Exception e) {
                            e.printStackTrace();
                        }
                        btnDelete.setVisibility(View.INVISIBLE);
                    }
                });
        // Setting Negative "Cancel" Button
        deleteDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to invoke NO event
                        dialog.cancel();
                    }
                });
        // Showing Alert Message
        deleteDialog.show();

    }

    public void openJobAssigned(View v) {

        lastVisitedPosition = -1;
        //Get Relative layout for submenu
        final RelativeLayout subMenu = (RelativeLayout) findViewById(R.id.sub_menu);
        //Hide the submenu
        subMenu.setVisibility(View.INVISIBLE);

        //Get listview relative layout
        final RelativeLayout listviewLayout = (RelativeLayout) findViewById(R.id.rl);

        final RelativeLayout.LayoutParams params=new RelativeLayout.LayoutParams((int) RelativeLayout.LayoutParams.WRAP_CONTENT,(int) RelativeLayout.LayoutParams.WRAP_CONTENT);
        //remove the views from listview layout
        listviewLayout.removeAllViews();
        //Declare the list as listview
        final ExpandableHeightListView list = new ExpandableHeightListView(this);
        //Set background color for list
        list.setBackgroundColor(Color.WHITE); //parseColor("#ffffff")
        //set background resource for rounded corner
        list.setBackgroundResource(R.drawable.list_rounded_corner);
        //list.setScrollingCacheEnabled(false);
        //list.setAnimationCacheEnabled(false);
        //list.setScrollContainer(false);
        list.setExpanded(true);
        //list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        // Each row in the list stores jobid, revision, jobdata, delete button
        List<HashMap<String,String>> aList = new ArrayList<HashMap<String,String>>();
        //Get the user data
        sharedpreferences = getSharedPreferences(dashboardActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        String strJson = sharedpreferences.getString("userJson", null); //getIntent().getStringExtra("userJson");
        //Initialize string data
        String data = "";
        //String strJson = "{\"jobs\": [{\"id\": 162,\"jobName\": \"newtest11\",\"jobNumber\": \"newtest11\",\"jobStatus\": \"IN_PROGRESS\",\"jobType\": 2,\"lastModifiedQcSpreadSheetDate\": 1434197427000,\"qcId\": 22,\"revision\": \"1\"},{\"id\": 102,\"jobName\": \"DAF-1\",\"jobNumber\": \"DAF-1\",\"jobStatus\": \"IN_PROGRESS\",\"jobType\": 2,\"lastModifiedQcSpreadSheetDate\": 1433928255000,\"qcId\": 22,\"revision\": \"1\"}],\"permissions\": \"VIEW_ALL_PIERS,VIEW_PIER,VIEW_DAILY_ACTIVITY,VIEW_JOB,VIEW_CAD_DRAWINGS,UPDATE_PROFILE,VIEW_ALL_USERS,VIEW_ALL_JOBS\",\"roles\": \"ROLE_READ_ONLY\",\"sessionToken\": \"597a530c-bdb6-4afa-ae0b-faa1d86bd3bc1440672684597\",\"success\": true,\"userId\": 22}";
        try {

            JSONObject jsonRootObject = new JSONObject(strJson);

            userId = Integer.parseInt(jsonRootObject.optString("userId"));
            //get session token
            sessionToken = jsonRootObject.optString("sessionToken");
            //get user role
            role = jsonRootObject.optString("roles");

            sharedpreferences = getSharedPreferences(dashboardActivity.MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("sessionToken", sessionToken);
            editor.putString("role", role);
            editor.putInt("userId", userId);
            editor.commit();

            //Get the instance of JSONArray that contains JSONObjects
            JSONArray jsonArray = jsonRootObject.optJSONArray("jobs");

            //Iterate the jsonArray and print the info of JSONObjects
            for(int i=0; i < jsonArray.length(); i++){

                JSONObject jsonObject = jsonArray.getJSONObject(i);

                int qcId = Integer.parseInt(jsonObject.optString("qcId"));

                if(userId == qcId) {
                    String jobName = jsonObject.optString("jobName");
                    String jobNumber = jsonObject.optString("jobNumber");
                    String jobId = jsonObject.optString("id");
                    String revision = jsonObject.optString("revision");

                    //Put data into list
                    HashMap<String, String> hm = new HashMap<String,String>();
                    hm.put("jobid", jobId);
                    hm.put("revision", revision);
                    hm.put("txt",jobName + " (" + jobNumber + ")");
                    hm.put("btn", "Delete Data");
                    aList.add(hm);
                }
            }

            // Keys used in Hashmap
            String[] from = { "jobid","revision","txt","btn" };
            // Ids of views in listview_layout
            int[] to = { R.id.job_id,R.id.job_revision,R.id.job_text,R.id.button_delete};
            // Instantiating an adapter to store each items
            // R.layout.listview_layout defines the layout of each item
            SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), aList, R.layout.listview_layout, from, to);
            adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            // Setting the adapter to the listView
            list.setAdapter(adapter);
            //Initialize params
            params.leftMargin=0;
            params.topMargin=0;
            //set layout params
            list.setLayoutParams(params);
            ColorDrawable divColor = new ColorDrawable(this.getResources().getColor(R.color.vertical_line));
            list.setDivider(divColor);
            list.setDividerHeight(1);

            list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    // Getting the Layout of the ListView
                    LinearLayout listLayout = (LinearLayout) view;
                    // Getting the Delete button
                    Button del = (Button) listLayout.getChildAt(2);
                    del.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteData(v, jobId);
                        }
                    });

                    if (del.getVisibility() == View.VISIBLE) {
                        del.setVisibility(View.INVISIBLE);
                        lastVisitedPosition = -1;
                    } else {
                        if (lastVisitedPosition != -1) {
                            Button btn = (Button) parent.getChildAt(lastVisitedPosition).findViewById(R.id.button_delete);
                            btn.setVisibility(View.INVISIBLE);
                        }
                        lastVisitedPosition = position;
                        del.setVisibility(View.VISIBLE);
                    }

                    //Getting the change qc button
                    Button change = (Button) listLayout.getChildAt(0);
                    change.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Todo
                        }
                    });
                    //change.setVisibility(View.VISIBLE);
                    return true;
                }
            });

            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                    Date t1 = new Date();
                    // set url string
                    String urlString = apiURL;
                    //set job type
                    jobType = "Assigned";
                    // Getting the Layout of the ListView
                    LinearLayout listLayout = (LinearLayout) v;
                    // Getting the Country TextView
                    TextView txtJobId = (TextView) listLayout.getChildAt(3);
                    //set job id
                    jobId = Integer.parseInt(txtJobId.getText().toString());
                    //Set the parameters for url
                    String paramsqry = "userId=" + userId + "&jobId=" + jobId;
                    // Getting the revision TextView
                    TextView revision = (TextView) listLayout.getChildAt(4);
                    // Getting the name TextView
                    TextView name = (TextView) listLayout.getChildAt(1);
                    //Set the parameters for url
                   // String jobDetail = revision.getText().toString() + "&" + name.getText().toString();

                    sharedpreferences = getSharedPreferences(dashboardActivity.MyPREFERENCES, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("revision", revision.getText().toString());
                    editor.putString("name", name.getText().toString());
                    editor.putString("jobType", jobType);
                    editor.putLong("timer1", t1.getTime());
                    editor.commit();
                    Date t2 = new Date();

                    //initialize offline data
                    String offlineUserData = "";

                    try {
                        //get offline data
                        //Open the user database
                        //SQLiteDatabase userdatabase = openOrCreateDatabase("GeoJobTrak", MODE_PRIVATE, null);
                        //If open the user database
                        //if(userdatabase != null) {

                            /*//Set the query
                            String qry = "SELECT * FROM jobDetail WHERE UserId="+userId+" AND JobId="+jobId;
                            //Select the user
                            Cursor resultSet = userdatabase.rawQuery(qry,null);
                            //Get the first record
                            if(resultSet.moveToFirst()) {
                                //Get the user details
                                offlineUserData = resultSet.getString(4);
                            }
                            userdatabase.close();*/
                        JobDetails job = new JobDetails();
                        job = new Select().from(JobDetails.class).where("UserId = ? AND JobId = ?", userId, jobId).executeSingle();
                        if (job != null) {
                            offlineUserData = job.jobs;
                        }
                        Date t3 = new Date();
                        //showPopup("Timer","Onclick: "+ (t2.getTime() - t1.getTime()) + " Database: " + (t3.getTime() - t2.getTime()));

                        //if offline data is available
                        if (offlineUserData != "") {

                            editor.putString("JobDetails", offlineUserData);
                            editor.commit();
                            //Redirect to JOB Details activity
                            Intent intent = new Intent(dashboardActivity.this, JobDetailsActivity.class);
                            //Pass data to next activity
                            //intent.putExtra("JobDetails", offlineUserData);
                            /*intent.putExtra("revision", revision.getText().toString());
                            intent.putExtra("name", name.getText().toString());*/
                            /*intent.putExtra("role", role);
                            intent.putExtra("userId", userId);
                            intent.putExtra("jobType", jobType);*/
                            startActivity(intent);
                        }
                        // }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Date t3 = new Date();
                    if (offlineUserData == "") {
                        Boolean offlineFlag = sharedpreferences.getBoolean("isOfflineUser", false);
                        if(offlineFlag) {
                            String user = sharedpreferences.getString("username", null);
                            String pwd = sharedpreferences.getString("password", null);
                            String para = "username="+user+"&password="+pwd;
                            //Call REST web service for authenticate the user
                            new RESTLoginWebservice().execute(loginURL, para+"&&"+paramsqry,urlString);
                        } else {
                            //Call rest web service
                            new RESTWebservice().execute(urlString,paramsqry);
                        }
                        //new RESTWebservice().execute(urlString, paramsqry, jobDetail);
                    }
                }
            });

            listviewLayout.addView(list);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void openJobOther(View v) {

        lastVisitedPosition = -1;
        //Get Relative layout for submenu
        final RelativeLayout subMenu = (RelativeLayout) findViewById(R.id.sub_menu);
        //Hide the submenu
        subMenu.setVisibility(View.INVISIBLE);

        //Get listview relative layout
        final RelativeLayout listviewLayout = (RelativeLayout) findViewById(R.id.rl);

        final RelativeLayout.LayoutParams params=new RelativeLayout.LayoutParams((int) RelativeLayout.LayoutParams.WRAP_CONTENT,(int) RelativeLayout.LayoutParams.WRAP_CONTENT);
        //remove the views from listview layout
        listviewLayout.removeAllViews();
        //Declare the list as listview
        final ExpandableHeightListView list = new ExpandableHeightListView(this);
        //Set background color for list
        list.setBackgroundColor(Color.WHITE); //parseColor("#ffffff")
        //set background resource for rounded corner
        list.setBackgroundResource(R.drawable.list_rounded_corner);
        //list.setScrollingCacheEnabled(false);
        //list.setAnimationCacheEnabled(false);
        list.setExpanded(true);
        // Each row in the list stores jobid, revision, jobdata, delete button
        List<HashMap<String,String>> aList = new ArrayList<HashMap<String,String>>();
        //Get the user data
        sharedpreferences = getSharedPreferences(dashboardActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        String strJson = sharedpreferences.getString("userJson", null);
        //String strJson = getIntent().getStringExtra("userJson");
        //Initialize string data
        String data = "";

        try {
            //Create JSON object from string
            JSONObject jsonRootObject = new JSONObject(strJson);
            //Get user id
            userId = Integer.parseInt(jsonRootObject.optString("userId"));
            //get session token
            sessionToken = jsonRootObject.optString("sessionToken");
            //get user role
            role = jsonRootObject.optString("roles");

            sharedpreferences = getSharedPreferences(dashboardActivity.MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("sessionToken", sessionToken);
            editor.putString("role", role);
            editor.putInt("userId", userId);
            editor.commit();

            //Get the instance of JSONArray that contains JSONObjects jobs
            JSONArray jsonArray = jsonRootObject.optJSONArray("jobs");

            //Iterate the jsonArray and print the info of JSONObjects jobs
            for(int i=0; i < jsonArray.length(); i++) {
                //get the job object
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                //get the qc id
                int qcId = Integer.parseInt(jsonObject.optString("qcId"));
                //If userid is not equal to qc id
                if(userId != qcId) {
                    String jobName = jsonObject.optString("jobName");
                    String jobNumber = jsonObject.optString("jobNumber");
                    String jobId = jsonObject.optString("id");
                    String revision = jsonObject.optString("revision");

                    //Put data into list
                    HashMap<String, String> hm = new HashMap<String,String>();
                    hm.put("jobid", jobId);
                    hm.put("revision", revision);
                    hm.put("txt",jobName + " (" + jobNumber + ")");
                    hm.put("btn", "Delete Data");
                    aList.add(hm);
                }

            }

            // Keys used in Hashmap
            String[] from = { "jobid","revision","txt","btn" };
            // Ids of views in listview_layout
            int[] to = { R.id.job_id,R.id.job_revision,R.id.job_text,R.id.button_delete};
            // Instantiating an adapter to store each items
            // R.layout.listview_layout defines the layout of each item
            SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), aList, R.layout.listview_layout, from, to);
            adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            // Setting the adapter to the listView
            list.setAdapter(adapter);
            //Initialize params
            params.leftMargin=0;
            params.topMargin=0;
            //set layout params
            list.setLayoutParams(params);
            ColorDrawable divColor = new ColorDrawable(this.getResources().getColor(R.color.vertical_line));
            list.setDivider(divColor);
            list.setDividerHeight(1);
            //On Item long click listener
            list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    // Getting the Layout of the ListView
                    LinearLayout listLayout = (LinearLayout) view;
                    // Getting the Delete button
                    Button del = (Button) listLayout.getChildAt(2);
                    del.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteData(v,jobId);
                        }
                    });

                    if(del.getVisibility() == View.VISIBLE) {
                        del.setVisibility(View.INVISIBLE);
                        lastVisitedPosition = -1;
                    } else {
                        if(lastVisitedPosition != -1){
                            Button btn = (Button) parent.getChildAt(lastVisitedPosition).findViewById(R.id.button_delete);
                            btn.setVisibility(View.INVISIBLE);
                        }
                        lastVisitedPosition = position;
                        del.setVisibility(View.VISIBLE);
                    }

                    // Getting the change qc button
                    Button change = (Button) listLayout.getChildAt(0);
                    change.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Todo
                        }
                    });
                    //change.setVisibility(View.VISIBLE);
                    return true;
                }
            });
            //on item click listener
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                    // set url string
                    String urlString = apiURL;
                    //set job Type
                    jobType = "Other";
                    // Getting the Layout of the ListView
                    LinearLayout listLayout = (LinearLayout) v;
                    // Getting the jobid TextView
                    TextView txtJobId = (TextView) listLayout.getChildAt(3);
                    jobId = Integer.parseInt(txtJobId.getText().toString());
                    //Set the parameters for url
                    String paramsqry = "userId=" + userId + "&jobId=" + jobId;
                    // Getting the revision TextView
                    TextView revision = (TextView) listLayout.getChildAt(4);
                    // Getting the name TextView
                    TextView name = (TextView) listLayout.getChildAt(1);
                    //Set the parameters for url
                    String jobRevision = revision.getText().toString();
                    //Set the parameters for url
                    String jobName = name.getText().toString();

                    sharedpreferences = getSharedPreferences(dashboardActivity.MyPREFERENCES, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("revision", revision.getText().toString());
                    editor.putString("name", name.getText().toString());
                    editor.putString("jobType", jobType);
                    editor.commit();

                    //initialize offline data
                    String offlineUserData = "";

                    try{

                        //get offline data
                        //Open the user database
                        //SQLiteDatabase userdatabase = openOrCreateDatabase("GeoJobTrak", MODE_PRIVATE, null);
                        //If open the user database
                        //if(userdatabase != null) {

                            //Set the query
                            //String qry = "SELECT * FROM jobDetail WHERE UserId="+userId+" AND JobId="+jobId;
                            //Select the user
                            //Cursor resultSet = userdatabase.rawQuery(qry,null);
                            //Get the first record
                           // if(resultSet.moveToFirst()) {
                                //Get the user details
                                //offlineUserData = resultSet.getString(4);
                            //}
                            //userdatabase.close();
                        JobDetails job = new JobDetails();
                        job = new Select().from(JobDetails.class).where("UserId = ? AND JobId = ?", userId, jobId).executeSingle();
                        if(job != null) {
                            offlineUserData = job.jobs;
                        }
                            //if offline data is available
                            if(offlineUserData != "") {

                                editor.putString("JobDetails",offlineUserData);
                                editor.commit();
                                //Redirect to JOB Details activity
                                Intent intent = new Intent(dashboardActivity.this, JobDetailsActivity.class);
                                //Pass data to next activity
                                //intent.putExtra("JobDetails",offlineUserData);
                               /* intent.putExtra("revision", jobRevision);
                                intent.putExtra("name", jobName);
                                intent.putExtra("role", role);
                                intent.putExtra("userId", userId);
                                intent.putExtra("jobType", jobType);*/
                                startActivity(intent);
                            }

                        //}
                    } catch(Exception e) {
                        e.printStackTrace();
                    }

                    if(offlineUserData == "") {
                        Boolean offlineFlag = sharedpreferences.getBoolean("isOfflineUser", false);
                        if(offlineFlag) {
                            String user = sharedpreferences.getString("username", null);
                            String pwd = sharedpreferences.getString("password", null);
                            String para = "username="+user+"&password="+pwd;
                            //Call REST web service for authenticate the user
                            new RESTLoginWebservice().execute(loginURL, para+"&&"+paramsqry,urlString);
                        } else {
                            //Call rest web service
                            new RESTWebservice().execute(urlString, paramsqry);
                        }
                        //new RESTWebservice().execute(urlString, paramsqry, jobRevision + "&" + jobName);
                    }

                }
            });
            //Add list into listview layout
            listviewLayout.addView(list);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void displayError(String err) {
        Toast.makeText(getApplicationContext(), err, Toast.LENGTH_SHORT).show();
    }

    private class RESTWebservice extends AsyncTask<String, String, String> {
        //Create a progress dialog
        private ProgressDialog Dialog = new ProgressDialog(dashboardActivity.this);
        //Declare a variable for url params
        String userData = "";
        //Declare a variable for Job's revision
        String jobRevision = "";
        //Declare a variable for //Job's name
        String jobName = "";
        //Declare a variable for store the response
        private String resultToDisplay = "";
        //Declare the variable for error
        private String Error = null;

        @Override
        protected void onPreExecute() {
            //Start Progress Dialog (Message)
            Dialog.setMessage("Please wait..");
            Dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            // URL to call
            String urlString = params[0];
            //post parameters
            userData = params[1];
            /*//details
            String [] details = params[2].split("&");
            //Jobs  revision
            jobRevision = details[0];
            //Jobs name
            jobName = details[1];*/
            //Declare a buffer reader
            BufferedReader reader=null;
            //Declare a URL connection
            HttpURLConnection urlConnection = null;
            // HTTP POST
            try {
                //set URL
                URL url = new URL(urlString);
                //Send POST data Request
                urlConnection = (HttpURLConnection) url.openConnection();
                //http POST method is used
                urlConnection.setDoOutput(true);
                //set content type
                urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                //set session token
                urlConnection.setRequestProperty("X-session-token", sessionToken);
                //Initialize output stream
                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                //set url parameters
                wr.write(userData);
                wr.flush();
                // Get the server response
                reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                // Read Server Response
                while((line = reader.readLine()) != null)
                {
                    // Append server response in string
                    sb.append(line + "  ");
                }
                // Append Server Response To String
                resultToDisplay = sb.toString();

            } catch (Exception e ) {
                System.out.println(e.getMessage());
                Error = e.getMessage();
            } finally {
                try
                {
                    if(reader != null) {
                        reader.close();
                    }
                } catch(Exception ex) { ex.printStackTrace(); }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String res) {
            // Close progress dialog
            Dialog.dismiss();

            if (Error != null) {
                displayError(Error);
            } else {
                try {
                    //Create json object from response
                    JSONObject jsonRootObject = new JSONObject(resultToDisplay);
                    //get status
                    boolean status = Boolean.parseBoolean(jsonRootObject.optString("success"));
                    //if request is not successful
                    if(status == false)
                    {
                        //Display the error message
                        String err = jsonRootObject.optString("error");
                        displayError(err);
                    } else {
                        //Store offline data
                        //SQLiteDatabase userdatabase = openOrCreateDatabase("GeoJobTrak", MODE_PRIVATE, null);
                        //userdatabase.setMaximumSize(userdatabase.getMaximumSize());
                        //if user database is created
                        //if(userdatabase != null) {
                            //Drop table
                            //userdatabase.execSQL("DROP TABLE IF EXISTS JobDetails");
                            //String exequery = "";
                            //Create table UserData
                            //userdatabase.execSQL("CREATE TABLE IF NOT EXISTS JobDetail(_id INTEGER PRIMARY KEY AUTOINCREMENT,JobId INTEGER,UserId INTEGER,JobType TEXT,JobDetails VARCHAR)");
                            //get user details, jobs
                            String jobDetails = resultToDisplay.toString();
                            JobDetails job = new JobDetails();
                            job.jobId = jobId;
                            job.userId = userId;
                            job.jobType = jobType;
                            job.jobs = jobDetails;
                            job.save();
                            //jobDetails.getBytes();
                            //Insert into database
                            //exequery = "INSERT INTO JobDetail(JobId,UserId,JobType,JobDetails) VALUES("+jobId+","+userId+",'"+jobType+"','"+jobDetails+"')";
                            //execute query
                            //userdatabase.execSQL(exequery);
                            //userdatabase.close();
                        //}
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                sharedpreferences = getSharedPreferences(dashboardActivity.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor .putString("JobDetails",resultToDisplay);
                editor.commit();

                //Redirect to JOB Details activity
                Intent intent = new Intent(dashboardActivity.this, JobDetailsActivity.class);
                //Pass data to next activity
                //intent.putExtra("JobDetails",resultToDisplay);
                //intent.putExtra("revision", jobRevision);
                //intent.putExtra("name", jobName);
                //intent.putExtra("role", role);
                //intent.putExtra("userId", userId);
                //intent.putExtra("jobType", jobType);
                startActivity(intent);
            }
        } //onPostExecute
    } // end CallAPI

    public void showPopup(String title, String pierNum) {
        if(!pierNum.equals("[]")) {
            AlertDialog.Builder piersPopup = new AlertDialog.Builder(dashboardActivity.this);
            piersPopup.setTitle(title);
            // Setting Dialog Message
            piersPopup.setMessage(pierNum);
            // Setting Positive "OK" Button
            piersPopup.setNegativeButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to invoke NO event
                            dialog.cancel();
                        }
                    });
            // Showing Alert Message
            piersPopup.show();
        }
    }

    private class RESTLoginWebservice extends AsyncTask<String, String, String> {
        //Initialize the variable for request params
        String userData = "";
        String resultToDisplay = "";
        String urlString = "";
        String paramsqry = "";
        String jobDetail = "";
        //Initialize the variable for error
        private String Error = null;

        @Override
        protected void onPreExecute() { }

        @Override
        protected String doInBackground(String... params) {
            //Get URL to call
            String urlLogin = params[0];
            String paramData[] = params[1].split("&&");
            userData = paramData[0];
            paramsqry = paramData[1];
            /*jobDetail = paramData[2];*/
            urlString = params[2];

            //Initialization of variables
            BufferedReader reader = null;
            HttpURLConnection urlConnection = null;
            // HTTP POST
            try {
                //Create url
                URL url = new URL(urlLogin);
                //Send POST data Request
                urlConnection = (HttpURLConnection) url.openConnection();
                //http POST method is used
                urlConnection.setDoOutput(true);
                //Set URL content type
                urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                //set URL params
                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(userData);
                wr.flush();
                // Get the server response
                reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                // Read Server Response
                while((line = reader.readLine()) != null)
                {
                    // Append server response in string
                    sb.append(line + "  ");
                }
                // Append Server Response To String
                resultToDisplay = sb.toString();
                //resultToDisplay = "{\"error\":\"Page not found\",\"errorCode\":\"404\",\"success\":false}";
            } catch (Exception e ) {
                Error = e.getMessage();
            } finally {
                try
                {
                    if(reader != null) {
                        reader.close();
                    }
                } catch(Exception ex) { ex.printStackTrace(); }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String res) {

            if (Error != null) {
                displayError(Error);
            } else {
                try {
                    //Create JSON object from response
                    JSONObject jsonRootObject = new JSONObject(resultToDisplay);
                    //Get status
                    boolean status = Boolean.parseBoolean(jsonRootObject.optString("success"));
                    //if request is failed
                    if(status == false)
                    {
                        int errorCode = Integer.parseInt(jsonRootObject.optString("errorCode"));
                        //Display the error message
                        displayError(jsonRootObject.optString("error"));
                        if(errorCode == 403) {
                            redirectTo();
                        }
                    } else {
                        sharedpreferences = getSharedPreferences(dashboardActivity.MyPREFERENCES, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putBoolean("isOfflineUser", false);
                        editor.commit();
                        //Call rest web service
                        new RESTWebservice().execute(urlString,paramsqry);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } //onPostExecute
    } // end CallAPI
}