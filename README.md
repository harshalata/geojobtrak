# README #

1. Tools: Android Studio
1.1 Download Android studio from https://developer.android.com/sdk/index.html.
1.2 Install and setup Android Studio and SDK packages as per steps given in above link.

2. Get the Source code of project from Downloads: Download repository.

3. Get the APK from Downloads: app-debug.apk to install on android mobile device.
This APK can be installed on mobile device API level 11 to 23 i.e. android version 3.0.x and above.

4. For test the app after installing on mobile device, 
Authentication credentials are 
username: testqc
password: qc

5. Versions attributes used in app's build.gradle file are:
5.1 Minimum Sdk Version is 11.
5.2 Target Sdk Version is 23.
5.3 Version Code is 1.
5.4 version Name "1.0".

6. Dependencies added in app's build.gradle file are:
1. 'com.android.support:appcompat-v7:23.0.0' 
2. 'com.michaelpardo:activeandroid:3.1.0-SNAPSHOT'

7. Database used is Active android.

Todo tasks:
Get details from Indrajit on
1. Creating pier History details.
2. Change log functionality after response from server.